import React from "react";
import { AppRegistry, View, StyleSheet, StatusBar, Image, Linking, TextInput, TouchableOpacity } from "react-native";
import {
  Button,
  Text,
  Container,
  Card,
  CardItem,
  Body,
  Content,
  Header,
  Left,
  Badge,
  Right,
  Icon,
  Title,
  Input,
  InputGroup,
  Item,
  Tab,
  Tabs,
  Footer,
  FooterTab,
  Label,
  Thumbnail,
  ListItem,
  Form,
  Picker
} from "native-base";
import { Slider, CheckBox } from 'react-native-elements';
import Modal from 'react-native-modal';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import { AppHeader, AppFooter } from '../app-nav/index';

export default class MenuSlider extends React.Component {



  constructor(props) {
    super(props);
    this.state = {
      isModalVisible: false,
      checked: false,
      value: 0,
      dataPOI: [],
      kategori: [],
      selected2: undefined
    };
  }

  _showModal = () => {

    this.setState({ isModalVisible: true })

  }

  dataradius = () => {
    navigator.geolocation.getCurrentPosition((position) => {
     this.setState({ position: { longitude: position.longitude, latitude: position.latitude } });
    return fetch("http://ec2-54-255-226-10.ap-southeast-1.compute.amazonaws.com:9009/api/foods", {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
      },
      body: JSON.stringify({
        gps: position.coords.latitude + ',' + position.coords.longitude,
        radius: this.state.value * 1300,
        types: this.state.selected2
      })
    })
      .then(response => response.json())
      .then((data) => {
        this.setState({
          dataPOI: data.data
        });
      })
    })
  }

  _hideModal = () => this.setState({ isModalVisible: false })
  checked = () => {
    if (this.state.checked == true) {
      this.setState({ checked: false })
    } else {
      this.setState({ checked: true })
    }
  }

  onValueChange2(value) {
    this.setState({
      selected2: value
    });
  }


  componentDidMount() {
    fetch("http://ec2-54-255-226-10.ap-southeast-1.compute.amazonaws.com:9009/api/category-google", {
      method: "GET",
      headers: {
        'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
      }
    })
      .then((response) => response.json())
      .then((data) => {

        this.setState({
          kategori: data.data,
          selected2: 'bank'
        });
        //console.log(kategori);
      })
      .catch((error) => {
        console.log(error);
      })
  }

  render() {
    return (
      <Container style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
        <AppHeader isMenu navigation={this.props.navigation} title="Point of Interest" />
        <Content>
          <Form>
            <Picker
              mode="dropdown"
              placeholder="Select One"
              selectedValue={this.state.selected2}
              onValueChange={this.onValueChange2.bind(this)}
            >
              {
                this.state.kategori.map((item, index) => (

                  <Item style={{ color: "white" }} label={item.categoryName} value={item.categoryValue} key={item._id} />

                ))
              }
            </Picker>
          </Form>
          <Card>
            <CardItem cardBody style={{ marginLeft: '5%', marginRight: '5%', justifyContent: 'space-between', flexDirection: "row" }}>
              <Text>Tentukan Radius</Text>
              <Text>{this.state.value} Km</Text>
            </CardItem>
            <CardItem>
              <View style={{ flex: 1, alignItems: 'stretch', justifyContent: 'center' }}>
                <Slider
                  step={1}
                  maximumValue={5}
                  value={this.state.value}
                  onValueChange={(value) => this.setState({ value })} />

                <Button primary style={{ marginTop: 10, height: 25 }} onPress={this.dataradius}>
                  <Text>Filter</Text>
                </Button>
              </View>
            </CardItem>
          </Card>
          {
            this.state.dataPOI.map((item, index) => (
              <TouchableOpacity key={index} onPress={() => this.props.navigation.navigate("Peta", { location : item })}>
                <Card>
                  <CardItem>
                    <Text style={{ fontSize: 15 }}>{item.name}</Text>
                  </CardItem>
                  <CardItem style={{ flexDirection: "row" }}>
                    <Image source={require("../../img/asset/home/lokasi.png")} />
                    <Body>
                      <Text style={{ fontSize: 12, marginLeft: 10 }}>{item.location}</Text>
                    </Body>
                  </CardItem>
                </Card>
              </TouchableOpacity>
            ))
          }
        </Content>
        <Modal isVisible={this.state.isModalVisible}>
          <Content padder style={{ position: "absolute", flex: 1, backgroundColor: 'rgba(255, 255, 255, 1)', marginRight: "10%", marginLeft: "10%", marginTop: "50%", marginBottom: "50%", borderRadius: 10 }}  >
            <CheckBox
              center
              title='Click Here'
              checked={this.state.checked}
              onPress={this.checked}
            />
            <Button onPress={this._hideModal} style={{ backgroundColor: "#0052A8", flex: 1, justifyContent: 'center', alignItems: 'center', marginLeft: "20%", marginRight: "25%", marginTop: "15%", width: responsiveWidth(40), borderRadius: 10 }}><Text> Oke </Text></Button>
          </Content>
        </Modal>
        {/*<AppFooter navigation={this.props.navigation} />*/}
      </Container>
    );
  }
}
{/*<TouchableOpacity onPress={() => this.props.navigation.navigate("Peta", { location: item })} >*/ }