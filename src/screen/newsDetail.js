import React from "react";
import { AppRegistry, View, StyleSheet, StatusBar, Image, Linking, TouchableHighlight } from "react-native";
import {
  Button,
  Text,
  Container,
  Card,
  CardItem,
  Body,
  Content,
  Header,
  Left,
  Badge,
  Right,
  Icon,
  Title,
  Input,
  InputGroup,
  Item,
  Tab,
  Tabs,
  Footer,
  FooterTab,
  Label,
  Thumbnail,
  ListItem,
  CheckBox,
  Picker,
  TouchableOpacity
} from "native-base";
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import { AppHeader, AppFooter } from '../app-nav/index';

export default class newsDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      databerita: '',
      id_berita: this.props.navigation.state.params.idBerita,
      shareFacebook: '',
      shareTwitter : ''

    };
  }
  
  componentDidMount() {
    fetch("http://ec2-13-229-200-215.ap-southeast-1.compute.amazonaws.com:5000/news_event_id/" + this.state.id_berita, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then((data) => {
        this.setState({
          databerita: data
        });
      })

      fetch("http://ec2-13-250-50-44.ap-southeast-1.compute.amazonaws.com:9009/api/shareFacebook", {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then((data) => {
        this.setState({
          shareFacebook: data.data
        });     
      })

      fetch("http://ec2-13-250-50-44.ap-southeast-1.compute.amazonaws.com:9009/api/shareTwitter", {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then((data) => {
        this.setState({
          shareTwitter: data.data
        });
      })

  }

  render() {
    return (
      <Container>
        
        <AppHeader navigation={this.props.navigation} title="Detail Berita" />
        
        <Content style={{backgroundColor: '#fff'}}>
          <Card style={{padding:'3%'}}>
            <Text style={styles.cardDate} >{this.state.databerita.newsStartDate}</Text>
            <CardItem cardBody>
              <Text style={styles.cardTitle} >{this.state.databerita.newsTitle}</Text>
            </CardItem>
            <CardItem cardBody>
              <Text style={styles.cardAuthor} >{this.state.databerita.newsAuthor}</Text>
            </CardItem>
             <CardItem cardBody>
              <TouchableHighlight style={{marginTop:"3%"}} underlayColor="white" onPress={() => Linking.openURL(this.state.shareFacebook)}>
                <Image style={{height:36,width:25}} source={require("../../img/asset/ic_facebook.png")} />
              </TouchableHighlight>
              <TouchableHighlight style={{marginTop:"3%"}} underlayColor="white" onPress={() => Linking.openURL(this.state.shareTwitter)}>
                <Image style={{height:36,width:24,marginLeft:7}} source={require("../../img/asset/ic_twitter.png")} />
              </TouchableHighlight>
              <TouchableHighlight style={{marginTop:"3%"}} underlayColor="white" >
                <Image style={{height:36,width:25,marginLeft:7}} source={require("../../img/asset/ic_whatsapp.png")} />
              </TouchableHighlight>
              <TouchableHighlight style={{marginTop:"3%"}} underlayColor="white" >
                <Image style={{height:36,width:24,marginLeft:7}} source={require("../../img/asset/ic_email.png")} />
              </TouchableHighlight>
              <TouchableHighlight style={{marginTop:"3%"}} underlayColor="white" >
                <Image style={{height:36,width:22,marginLeft:7}} source={require("../../img/asset/ic_copy.png")} />
              </TouchableHighlight>
           </CardItem>
            <Thumbnail square source={{uri : this.state.databerita.newsImage }} style={styles.cardImage} />
            <CardItem cardBody>
              <Text style={styles.cardDesc} >{this.state.databerita.newsDescription}</Text>
            </CardItem>
          </Card>
        </Content>
      </Container>

    );
  }
}

function parseDate(data) {
  if (!data) return "";
  const date = new Date(data);
  let retval = "";
  switch (date.getDay()) {
    case 0: retval+= "Senin"; break;
    case 1: retval+= "Selasa"; break;
    case 2: retval+= "Rabu"; break;
    case 3: retval+= "Kamis"; break;
    case 4: retval+= "Jumat"; break;
    case 5: retval+= "Sabtu"; break;
    case 6: retval+= "Minggu"; break;
  }
  retval += ", " + date.getDate();
  switch (date.getMonth()) {
    case 0: retval+= " Jan"; break;
    case 1: retval+= " Feb"; break;
    case 2: retval+= " Mar"; break;
    case 3: retval+= " Apr"; break;
    case 4: retval+= " Mei"; break;
    case 5: retval+= " Jun"; break;
    case 6: retval+= " Jul"; break;
    case 7: retval+= " Agu"; break;
    case 8: retval+= " Sep"; break;
    case 9: retval+= " Okt"; break;
    case 10: retval+= " Nov"; break;
    case 11: retval+= " Des"; break;
  }
  retval += " " + date.getFullYear();
  return retval;
}

const styles = StyleSheet.create({
  cardImage: { 
    width:'100%',
    height:150,
    borderRadius:3,
    marginTop:'3%',
  },
  cardDate: {
    marginTop:'2%',
    fontFamily:'Ubuntu_Light',
    fontSize: 10,
  },
  cardTitle: {
    fontFamily:'Gotham_Bold',
    fontSize: 15,
  },
  cardAuthor: {
    fontFamily:'Ubuntu_Light',
    fontSize: 12,
  },
  cardDesc: {
    marginTop:'2%',
    fontFamily:'Ubuntu_Light',
    fontSize: 14,
    lineHeight:16,
  }
});
